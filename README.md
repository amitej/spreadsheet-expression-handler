# spreadsheet-expression-handler
   
Initial version to accept csv as an input, processes various arithmetic and referential expressions and generate final output in csv format.
   
## Pre-requisite
   
Maven 3+ and Java 8 or IDE as per your preference.
   
## Installation
   
Download the source and extract to desired folder. For IDE use new project from version control > Git and mention repository url. Once cloned/or opened from extracted folder IDE will prompt to add as maven project. Once completed processing add new configuration for maven with command line 'clean install'
   
For command line follow below steps
   
```bash
   
cd extractedfolder
mvn install
   
```
   
Output : spreadsheet.jar will be generated in extracted folder.
   
## Usage
   
Run program using below command line
   
```bash
   
java -jar spreadsheet.jar -i input.csv -o output.csv
   
```
   
