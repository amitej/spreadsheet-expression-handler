import org.junit.jupiter.api.Test;

import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

class EvaluateExpressionTest {

    @Test
    void eval() {
        java.util.TreeMap<String,Double> value = new TreeMap<>();
        java.util.TreeMap<String,String> expression = new TreeMap<>();
        value.put("A1",20.00);
        value.put("B1",10.00);
        expression.put("B2","A1*(A1+B1)*1");
        EvaluateExpression e = new EvaluateExpression(value,expression);
        assertEquals("{A1=20.0, B1=10.0, B2=600.0}",e.eval().toString());
    }
}