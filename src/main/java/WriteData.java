import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class WriteData {
    private String fileName;

    public WriteData(String fileName){
        this.fileName=fileName;
    }

    public void writetofile(TreeMap<String, Double> toWrite) {
        PrintWriter writer = null;
        try {
            File file = new File(fileName);
            file.getParentFile().mkdirs();
            writer = new PrintWriter(file);
            char curr = 'A';
            ArrayList<String> val = new ArrayList<>();
            DecimalFormat df = new DecimalFormat("0.0000");
            for (String cell:toWrite.keySet()
                 ) {
                if (cell.charAt(0)!=curr) {
                    writer.println(val.stream().collect(Collectors.joining(",")));
                    val.clear();
                    curr = cell.charAt(0);
                }
                val.add(df.format(toWrite.get(cell)));
            }
            writer.print(val.stream().map(String::valueOf).collect(Collectors.joining(",")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}
