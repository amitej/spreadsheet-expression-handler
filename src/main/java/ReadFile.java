import java.io.*;
import java.util.concurrent.BlockingQueue;

public class ReadFile implements Runnable{

    protected BlockingQueue<String> blockingQueue = null;
    protected String filePath = null;

    public ReadFile(BlockingQueue<String> blockingQueue,String filePath){
        this.blockingQueue = blockingQueue;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(filePath)));
            String buffer =null;
            while((buffer=br.readLine())!=null){
                blockingQueue.put(buffer);
            }
            blockingQueue.put("EOF");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch(InterruptedException e){

        }finally{
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
