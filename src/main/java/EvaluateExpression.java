import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EvaluateExpression {

    private TreeMap<String,Double> evaluatedData;
    private TreeMap<String,String> expression;

    public EvaluateExpression(TreeMap<String,Double> value,TreeMap<String,String> expression){
        this.evaluatedData=value;
        this.expression=expression;
    }

    public TreeMap<String, Double> eval(){
        for (String key:expression.keySet()
        ) {
            evaluatedData.put(key,evalExpression(buildExpression(expression.get(key))));
        }
        return evaluatedData;
    }

    private String buildExpression(String input){
        Pattern pattern = Pattern.compile("([A-Z][0-9]*)");
        Matcher matcher = pattern.matcher(input);
        String toEval = input;
        while (matcher.find()){
            String reference = matcher.group(1);
            Double value = evaluatedData.get(reference);
            if(value != null) {
                toEval=toEval.replace(reference,String.valueOf(value));
            }else{
                try {
                    throw new Exception("Circular reference found. Unable to evaluate.");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        }
        return toEval;
    }

    private double evalExpression(final String str) {
        return new Object() {
            int pos = -1, ch;
            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }
            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }
            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }
                return x;
            }
        }.parse();
    }
}
