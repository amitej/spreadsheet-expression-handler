import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.*;

public class spreadsheet {

    public static void main(String[] args) {
        if(args.length != 4){
            System.out.println("Invalid number of arguments");
            printHelp();
            System.exit(1);
        }
        String input = null;
        String output = null;
        for ( int i = 0; i < 4; i++ ) {
            if ( args[i].equals("-i") ) {
                i++;
                input = new File(args[i]).getAbsolutePath();
            } else if ( args[i].equals("-o") ) {
                i++;
                output = new File(args[i]).getAbsolutePath();
            } else {
            }
        }
        if(Files.notExists(Paths.get(input))){
            System.out.println("Error : Input " + input + " file not found.");
            System.exit(1);
        }

        BlockingQueue<String> queue = new ArrayBlockingQueue<String>(1024);
        ReadFile reader = new ReadFile(queue,input);
        ProcessRawData processRawData = new ProcessRawData(queue);
        new Thread(reader).start();
        Thread processData = new Thread(processRawData);
        processData.start();
        try {
            processData.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EvaluateExpression e = new EvaluateExpression(processRawData.getCellDataValue(),processRawData.getCellDataExp());
        WriteData writeData = new WriteData(output);
        writeData.writetofile(e.eval());
    }

    private static void printHelp(){
        System.out.println("Help : ");
        System.out.println("Expected Syntax : java -jar spreadsheet.jar -i input.csv -o output.csv");
    }
}
