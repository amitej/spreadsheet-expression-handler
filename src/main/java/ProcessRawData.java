import java.text.DecimalFormat;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;

public class ProcessRawData implements Runnable {

    protected BlockingQueue<String> blockingQueue = null;

    public TreeMap<String, Double> getCellDataValue() {
        return cellDataValue;
    }

    public TreeMap<String, String> getCellDataExp() {
        return cellDataExp;
    }

    private TreeMap<String, Double> cellDataValue = new TreeMap<String, Double>();
    private TreeMap<String, String> cellDataExp = new TreeMap<String, String>();
    private int index = 1;

    public ProcessRawData(BlockingQueue<String> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String buffer = blockingQueue.take();
                if (buffer.equals("EOF")) {
                    break;
                }
                splitValues(buffer);
                index++;
            }
        } catch (InterruptedException e) {}
    }

    private void splitValues(String line) {
        String[] cellValues = line.split(",");
        String rowName = getAlphabet(index);
        DecimalFormat df = new DecimalFormat("#.####");
        if (rowName != null) {
            int colIndex = 0;
            for (String data : cellValues
            ) {
                if (data.charAt(0) == '=') {
                    cellDataExp.put(rowName + colIndex, data.split("=", 2)[1]);
                } else {
                    cellDataValue.put(rowName + colIndex, Double.parseDouble(data));
                }
                colIndex++;
            }
        }

    }
    private String getAlphabet(int i){
        return i > 0 && i < 27 ? String.valueOf((char)(i+'A'-1)) : null;
    }
}
